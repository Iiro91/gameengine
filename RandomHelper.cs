using System;

public class RandomHelper
{
    private static Random random;

    static RandomHelper()
    {
        random = new Random();
    }

    public static int getRandomInt(int min, int max)
    {
        return random.Next(min, max);
    }

    public static string generateRandomString(int strLength)
    {
        if (strLength < 1) {
            return "";
        } 
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var stringChars = new char[strLength];

        for (int i = 0; i < stringChars.Length; i++)
        {
            stringChars[i] = chars[random.Next(chars.Length)];
        }

        return new String(stringChars);
    }
}