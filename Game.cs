using System;
using System.Collections.Generic;
using GameEngine.Units;

public class Game
{
    public List<GameObject> gameObjects { get; set; }
    public List<Coordinates> coordinatesList { get; set; }

    public Game()
    {
        this.gameObjects = new List<GameObject>();
        this.coordinatesList = new List<Coordinates>();
    }

    public void Update(TimeSpan gameTime)
    {
        // // Gametime elapsed
        //double gameTimeElapsed = gameTime.TotalMilliseconds / 1000;
        double gameTimeElapsed = gameTime.TotalMilliseconds / 1000;
        //Console.WriteLine(gameTimeElapsed);

        Coordinates coordinates = new Coordinates{
            x = RandomHelper.getRandomInt(0, 500),
            y = RandomHelper.getRandomInt(0, 500),
        };
        gameObjects.Add(new Infantry(coordinates));
        coordinatesList.Add(coordinates);
    }

    public void testi()
    {
        Console.WriteLine("TESTI");
    }
}