using System;
using System.Threading.Tasks;

public class GameLoop
{
    private Game _myGame;

    public bool Running { get; private set; }

    public GameLoop(Game gameObj)
    {
        this._myGame = gameObj;
    }

    public async void Start()
    {
        if (this._myGame == null)
            throw new ArgumentException("Game not loaded!");

        // Load game content
        //_myGame.Load();

        // Set gameloop state
        this.Running = true;

        // Set previous game time
        DateTime _previousGameTime = DateTime.Now;

        while (this.Running)
        {
            // Calculate the time elapsed since the last game loop cycle
            TimeSpan GameTime = DateTime.Now - _previousGameTime;
            // Update the current previous game time
            _previousGameTime = _previousGameTime + GameTime;
            // Update the game
            _myGame.Update(GameTime);
            Console.WriteLine("LOOP");
            // Update Game at 60fps
            //await Task.Delay(8); // 60 FPS
            await Task.Delay(1000); // Every second
        }
    }

    public void Stop()
    {
        this.Running = false;
        //_myGame?.Unload();
    }
}