namespace GameEngine.Units
{
    public class Infantry : Unit
    {
        public Infantry(Coordinates coordinates)
        {
            this.Name = "Infantry";
            this.Ammo = 90;
            this.Coordinates = coordinates;
        }
    }
}