﻿using System;

public class GameCore
{
    public static Game Game;
    public static GameLoop GameLoop;

    static GameCore()
    {
        Game = new Game();
        GameLoop = new GameLoop(Game);
    }

    public static void run()
    {
        // System.Threading.Tasks.Task.Run(() => InsertAnsweredQuestion(_serviceFactory, data));
        GameLoop.Start();
    }

    protected void test() 
    {
        //TimeSpan
    }
}