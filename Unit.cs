public class Unit : GameObject
{
    public string Name { get; set; }
    public int Amount { get; set; }
    public int Ammo { get; set; }
    public string AvailableMovements { get; set; }
    
}