using System;

namespace GameEngine
{
    class EnvStatusInfo
    {
        public string EnvName { get; set; }
        public DateTime StartTime { get; set; }
    }
}