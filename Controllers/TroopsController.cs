﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GameEngine.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TroopsController : ControllerBase
    {

        private readonly ILogger<TroopsController> _logger;

        public TroopsController(ILogger<TroopsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<bool> Get()
        {
            //return Ok(GameCore.GameLoop.Running);
            return Ok(true);
        }

        [HttpGet, Route("get")]
        public ActionResult GetA()
        //public ActionResult<List<GameObject>> GetA()
        {
            string jsonString = JsonSerializer.Serialize(GameCore.Game.gameObjects);
            Console.WriteLine(jsonString);
            //return Ok(GameCore.GameLoop.Running);
            //return Ok(GameCore.Game.gameObjects);
            return Ok(GameCore.Game.coordinatesList);
        }
    }
}
