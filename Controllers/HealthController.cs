using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

using Microsoft.AspNetCore.Hosting;
using System.Diagnostics;

namespace GameEngine.Controllers
{
    [Route("[controller]")]
    public class HealthController : ControllerBase
    {
        private readonly AppSettings _appSettings;

        private readonly IWebHostEnvironment _env;

        public HealthController(
            IOptions<AppSettings> appSettings,
            IWebHostEnvironment env
            )
        {
            _appSettings = appSettings.Value;
            _env = env;
        }

        [HttpGet]
        public ActionResult Index()
        {
            EnvStatusInfo info = new EnvStatusInfo();
            info.EnvName = _env.EnvironmentName;
            info.StartTime = Process.GetCurrentProcess().StartTime;
            return Ok(info);
        }
    }
}