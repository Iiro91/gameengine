using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GameEngine.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BuildingsController : ControllerBase
    {

        private readonly ILogger<BuildingsController> _logger;

        public BuildingsController(ILogger<BuildingsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<bool> Get()
        {
            return Ok(true);
        }
    }
}
